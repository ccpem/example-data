# Python script to run a com file within a defined loop.
# Alan Roseman 2002
# eg:  python loopthroughacomfile.py NUM 1 10 goFindEM NUM
# goFindEM NUM is executed 10 times with NUM incrementing from 1 to 10.
# NUM can be any character sequence, can be part of a string also, eg
#  filenameNUM.spi, and it can occur more than once in the command line.
import sys
import os
import string
print sys.argv
arglen=len(sys.argv)
var = sys.argv[1]
st = sys.argv[2]
fin = eval(sys.argv[3])
rest = sys.argv[4:arglen] 
rest2 = rest
length = len(var)
len2 = len(rest)
cnt = eval(st)
print var,st,fin,arglen
while cnt <= fin:
	print cnt	
	rest2 = string.join(rest)
	c=repr(cnt)
	fcnt=string.zfill(c,length)	
	var1=var
	
	rest3 = string.replace(rest2,var1,fcnt)	

	command = rest3
	
	print command
	os.system (command)
	cnt = cnt + 1

sys.exit()
