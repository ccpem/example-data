
/* ================================================================ .
 *                                                                  *
 *  EXAMPLE 01:  Plain HTML layout: Text                            *
 *                                                                  *
 ' ================================================================ */

#include <unistd.h>
#include <sys/stat.h> // mkdir

#include "../rvapi/src/rvapi_interface.h"


int main ( int argc, char ** argv, char ** env )  {
int   delay = 2;  // seconds, to imitate a runnning process

  // create report directory just in case
  mkdir ( "report",0777 );

  // initialise document first
  rvapi_init_document ( "demo",               // mandatory, use any name
                        "./report",           // mandatory, use any existing output directory name
                        "Plain HTML Layout: Text",  // mandatory, use any page title
                        RVAPI_MODE_Html,      // mandatory, use RVAPI_MODE_Html
                        RVAPI_LAYOUT_Plain,   // mandatory, request plain page
                        "../../rvapi/jsrview", // needed, either a URL, or absolute
                                               // path or path relative to
                                               // "./report"
                        NULL,                 // help file not applicable to basic layout
                        "index.html",         // may be NULL, then "index.html" is used
                        NULL,                 // default task file name (task.tsk)
                        NULL                  // special use for CCP4i2, ignore
                     );
  rvapi_flush();  // generate empty index.html at once

  // put title
  rvapi_set_text    ( "<h2>PLAIN HTML PAGE OUTPUT: TEXT</h2>",  // title
                      "body",               // root grid for plain page layout
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( 3*delay );  // give 5 secs to manually reload page in browser
  rvapi_flush();

  // add free text
  rvapi_set_text    ( "This is a demo of basic HTML layout. 'Basic' means that "
                      "all output will be handled in a plain page, without "
                      "predefined title, toolbar and tabs. All other RVAPI "
                      "elements, for example, sections, panels, texts, tables "
                      "graphs etc. can be added on the top-level grid referenced "
                      " as 'body'.",
                      "body",               // root grid for plain page layout
                      1,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );
  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();

  return 0;

}
