
/* ================================================================ .
 *                                                                  *
 *  EXAMPLE 09:  Plain HTML layout: Trees                            *
 *                                                                  *
 ' ================================================================ */

#include <unistd.h>
#include <sys/stat.h> // mkdir

#include "../rvapi/src/rvapi_interface.h"


int main ( int argc, char ** argv, char ** env )  {
int   delay = 2;  // seconds, to imitate a runnning process

  // create report directory just in case
  mkdir ( "report",0777 );

  // initialise document first
  rvapi_init_document ( "demo",               // mandatory, use any name
                        "./report",           // mandatory, use any existing output directory name
                        "Plain HTML Layout: Trees",  // mandatory, use any page title
                        RVAPI_MODE_Html,      // mandatory, use RVAPI_MODE_Html
                        RVAPI_LAYOUT_Plain,   // mandatory, request plain page
                        "../../rvapi/jsrview", // needed, either a URL, or absolute
                                               // path or path relative to
                                               // "./report"
                        NULL,                 // help file not applicable to basic layout
                        "index.html",         // may be NULL, then "index.html" is used
                        NULL,                 // default task file name (task.tsk)
                        NULL                  // special use for CCP4i2, ignore
                     );
  rvapi_flush();  // generate empty index.html at once

  // put title
  rvapi_set_text    ( "<h2>PLAIN HTML PAGE OUTPUT: TREES</h2>",  // title
                      "body",               // root grid for plain page layout
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( 3*delay );  // give 5 secs to manually reload page in browser
  rvapi_flush();

  // Add tree widget

  rvapi_add_tree_widget ( "tree",          // tree widget reference (Id)
                          "Tree Example",  // tree widget title
                          "body",          // reference to widget holder (grid)
                          1,               // holder row
                          0,               // holder column
                          1,               // row span
                          1                // column span
                        );

  // Tree widget may be though of as a grid with cell contents switchable by
  // selections of tree leafs. The set of contentes should be defined first,
  // and mapping of contents to tree nodes should be done second.

  // Define tree contents now. In this example, each tree leaf will be mapped
  // onto a section widget (another suitable widget would be Panel). Simply put
  // section widgets into the tree widget. Note that sections will occupy
  // same grid cells.

  // Put section for 1st tree leaf
  rvapi_add_section ( "sec_node1",           // section reference (Id)
                      "Section for NODE 1",  // section title
                      "tree",                // reference to section holder widget
                      0,                     // holder row
                      0,                     // holder column
                      1,                     // row span
                      1,                     // column span
                      true                   // section initially open
                    );
  // Put some contents into section
  rvapi_add_text    ( "Text for NODE 1",     // Text
                      "sec_node1",           // reference to text holder
                      0,                     // holder row
                      0,                     // holder column
                      1,                     // row span
                      1                      // column span
                    );
  // Now add other sections and their contents
  rvapi_add_section ( "sec_node2","Section for NODE 2","tree",0,0,1,1,true );
  rvapi_add_text    ( "Text for NODE 2","sec_node2",0,0,1,1 );
  rvapi_add_section ( "sec_node21","Section for NODE 21","tree",0,0,1,1,true );
  rvapi_add_text    ( "Text for NODE 21","sec_node21",0,0,1,1 );
  rvapi_add_section ( "sec_node22","Section for NODE 22","tree",0,0,1,1,true );
  rvapi_add_text    ( "Text for NODE 22","sec_node22",0,0,1,1 );

  // Now construct the tree and map sections to tree leafs

  rvapi_set_tree_node ( "tree",       // reference to tree widget (widget's Id)
                        "sec_node1",  // reference to leaf contents
                        "Node 1",     // tree leaf title
                        "auto",       // manage the leaf open state automatically
                        ""            // parent leaf Id; empty string means
                                      //   a top-level node; nodes with same
                                      //   parent Id appear in order of  their
                                      //   definitions
                      );
  // set leaf 2 on top level and make it open by default
  rvapi_set_tree_node ( "tree","sec_node2","Node 2","open","" );
  // make leafs 21 and 22 chldren of leaf 2
  rvapi_set_tree_node ( "tree","sec_node21","Node 21 make this long","auto","sec_node2" );
  rvapi_set_tree_node ( "tree","sec_node22","Node 22","auto","sec_node2" );

  return 0;

}
