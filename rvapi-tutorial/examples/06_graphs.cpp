
/* ================================================================ .
 *                                                                  *
 *  EXAMPLE 06:  Plain HTML layout: Graphs                          *
 *                                                                  *
 ' ================================================================ */

#include <math.h>
#include <unistd.h>
#include <sys/stat.h> // mkdir
#include <stdio.h>

#include "../rvapi/src/rvapi_interface.h"


int main ( int argc, char ** argv, char ** env )  {
int   delay = 2;  // seconds, to imitate a runnning process

  // create report directory just in case
  mkdir ( "report",0777 );

  // initialise document first
  rvapi_init_document ( "demo",               // mandatory, use any name
                        "./report",           // mandatory, use any existing output directory name
                        "Plain HTML Output: Graphs",  // mandatory, use any page title
                        RVAPI_MODE_Html,      // mandatory, use RVAPI_MODE_Html
                        RVAPI_LAYOUT_Plain,   // mandatory, request plain page
                        "../../rvapi/jsrview", // needed, either a URL, or absolute
                                               // path or path relative to
                                               // "./report"
                        NULL,                 // help file not applicable to basic layout
                        "index.html",         // may be NULL, then "index.html" is used
                        NULL,                 // default task file name (task.tsk)
                        NULL                  // special use for CCP4i2, ignore
                     );
  rvapi_flush();  // generate empty index.html at once

  // put title
  rvapi_set_text    ( "<h2>PLAIN HTML PAGE OUTPUT: GRAPHS</h2>",  // page title
                      "body",               // root grid for plain page layout
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( 3*delay );  // give 5 secs to manually reload page in browser
  rvapi_flush();

  rvapi_add_graph  ( "graph",    // new graph reference
                     "body",     // will put the graph into "body" grid
                     1,          // grid row
                     0,          // grid column
                     1,          // row span
                     1           // column span
                    );

  // Graph widgets contain lists of data and lists of plots. Here, we
  // describe a single data block, containing x-points and 4 functions
  // on them. Note that any ids for X- and Y- values may be chosen, and
  // that they are local to the data block.

  rvapi_add_graph_data ( "data",         // data block reference
                         "graph",        // graph widget reference
                         "Trigonometry"  // data block title
                       );

  // Now define datasets, or vectors of data points. Any of them may be used
  // as either X or Y values, subject to definitions of plot lines, below
  rvapi_add_graph_dataset ( "x",         // dataset reference
                            "data",      // data block reference
                            "graph",     // graph widget reference
                            "x",         // data set name (used in plots)
                            "argument"   // data set title (used in choice menus)
                          );
  rvapi_add_graph_dataset ( "y1","data","graph","sin(x)","Sine"   );
  rvapi_add_graph_dataset ( "y2","data","graph","cos(x)","Cosine" );
  rvapi_add_graph_dataset ( "y3","data","graph","sin(x)/x","Damped sine"   );
  rvapi_add_graph_dataset ( "y4","data","graph","cos(x)/x","Damped cosine" );

  // Now put data into datasets
  for (int i=1;i<21;i++)  {
    rvapi_add_graph_int ( "x",     // dataset reference
                          "data",  // data block reference
                          "graph", // graph widget reference
                          i        // value
                        );
    rvapi_add_graph_real ( "y1",              // dataset reference
                           "data",            // data block reference
                           "graph",           // graph widget reference
                           sin(i*6.28/19.0),  // value
                           "%g"               // data format to use in JS layer
                         );
    rvapi_add_graph_real ( "y2","data","graph",cos(i*6.28/19.0),"%g" );
    rvapi_add_graph_real ( "y3","data","graph",sin(i*6.28/19.0)/i,"%g" );
    rvapi_add_graph_real ( "y4","data","graph",cos(i*6.28/19.0)/i,"%g" );
  }

  // Define plot area. In general, graph widget can have multiple plots, but
  // here we will use just a single one. See use of multiple plots in Example 07.

  rvapi_add_graph_plot ( "plot",    // plot reference
                         "graph",   // graph widget reference
                         "Sines and Cosines",  // plot title
                         "Argument",    // X-axis name
                         "Functions"    // Y-axis name
                       );

  // Now define lines in the plot

  rvapi_add_plot_line ( "plot",      // plot reference
                        "data",      // data block reference
                        "graph",     // graph widget reference
                        "x",         // reference of dataset to be used as X
                        "y1"         // reference of dataset to be used as Y
                      );
  rvapi_add_plot_line ( "plot","data","graph","x","y2" );
  rvapi_add_plot_line ( "plot","data","graph","x","y3" );
  rvapi_add_plot_line ( "plot","data","graph","x","y4" );

  // Finally, push changes into the browser.
  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();

  rvapi_set_text ( "<p>&nbsp;<p>"
                   "The graph may fine-tuned using many additional functions "
                   "in <i>interface.h</i>: pixel dimensions, X/Y ranges, "
                   "logarithimc<br>scales, forced integer scales, line style "
                   "and colours, initial slider positions, legends and other "
                   "parameters can be<br>customised.",
                   "body",2,0,1,1 );

  return 0;

}
