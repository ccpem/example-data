
/* ================================================================ .
 *                                                                  *
 *  EXAMPLE 02:  Plain HTML layout: Tables                          *
 *                                                                  *
 ' ================================================================ */

#include <unistd.h>
#include <sys/stat.h> // mkdir
#include <stdio.h>

#include "../rvapi/src/rvapi_interface.h"


int main ( int argc, char ** argv, char ** env )  {
int   delay = 2;  // seconds, to imitate a runnning process

  // create report directory just in case
  mkdir ( "report",0777 );

  // initialise document first
  rvapi_init_document ( "demo",               // mandatory, use any name
                        "./report",           // mandatory, use any existing output directory name
                        "Plain HTML Output: Table",  // mandatory, use any page title
                        RVAPI_MODE_Html,      // mandatory, use RVAPI_MODE_Html
                        RVAPI_LAYOUT_Plain,   // mandatory, request plain page
                        "../../rvapi/jsrview", // needed, either a URL, or absolute
                                               // path or path relative to
                                               // "./report"
                        NULL,                 // help file not applicable to basic layout
                        "index.html",         // may be NULL, then "index.html" is used
                        NULL,                 // default task file name (task.tsk)
                        NULL                  // special use for CCP4i2, ignore
                     );
  rvapi_flush();  // generate empty index.html at once

  // put title
  rvapi_set_text    ( "<h2>PLAIN HTML PAGE OUTPUT: TABLES</h2>",  // title
                      "body",               // root grid for plain page layout
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( 3*delay );  // give 5 secs to manually reload page in browser
  rvapi_flush();

  // add table
  rvapi_add_table ( "my_table",   // table Id
                    "Table with horizontal and vertical headers",  // title
                    "body",       // will place in the top-level grid (table
                                  // holder)
                    1,            // grid row,
                    0,            // grid column
                    1,            // row span,
                    1,            // column span
                    1             // this is inital fold state:
                                  //   0: the table is not foldable
                                  // 100: the table is not foldable and spans to
                                  //      the whole width of table holder
                                  //  -1: the table is foldable and is initially
                                  //      folded
                                  //   1: the table is foldable and is initially
                                  //      unfolded
                                  //  -2: the table is foldable and is initially
                                  //      folded and spans to the whole width of
                                  //      table holder
                                  //   2: the table is foldable and is initially
                                  //      unfolded and spans to the whole width
                                  //      of table holder
                  );

  // Make column headers (optional)
  rvapi_put_horz_theader ( "my_table","Column 1","Tooltip 1",0 );
  rvapi_put_horz_theader ( "my_table","Column 2","",1 );
  rvapi_put_horz_theader ( "my_table","Column 3","Tooltip 2",2 );
  rvapi_put_horz_theader ( "my_table","Column 4","",3 );

  // Make row headers (optional)
  rvapi_put_vert_theader ( "my_table","Row 1","Tooltip 1",0 );
  rvapi_put_vert_theader ( "my_table","** Row 2 **","",1 );

  // Fill table body. Any strings may be put in, however, there are
  // interface functions for other types of data. Note that one
  // can leave empty cells and intersperse data types at will
  for (int trow=0;trow<2;trow++)
    for (int tcol=0;tcol<4;tcol++)  {
      char S[100];
      sprintf ( S,"%10.3g",float(trow+1)*float(tcol+1)/3.14159265 );
      rvapi_put_table_string ( "my_table",S,trow,tcol );
    }

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();

  // stretch the last column and put a tooltip: see effect in browser
  rvapi_shape_table_cell ( "my_table",
                           0,  // row
                           3,  // column
                           "tooltip",
                           "width:100%;",
                           "",  // custom style record if specified
                           1,   // row span
                           1    // column span
                         );

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();



  // Now make table that does not span the whole width. In order to achieve
  // compact placement, nest another grid in the "body"

  rvapi_add_grid     ( "my_grid",   // grid Id
                        true,       // fill the width
                        "body",     // root grid for plain page layout
                        2,          // grid row,
                        0,          // grid column
                        1,          // row span,
                        1           // column span
                      );

  // add table
  rvapi_add_table ( "my_compact_table",   // table Id
                    "Same table as above but optimised to the left",  // title
                    "my_grid",    // will place in the custom grid
                    0,            // grid row,
                    0,            // grid column
                    1,            // row span,
                    1,            // column span
                    1             // this is inital fold state:
                                  //   0: the table is not foldable
                                  // 100: the table is not foldable and spans to
                                  //      the whole width of table holder
                                  //  -1: the table is foldable and is initially
                                  //      folded
                                  //   1: the table is foldable and is initially
                                  //      unfolded
                                  //  -2: the table is foldable and is initially
                                  //      folded and spans to the whole width of
                                  //      table holder
                                  //   2: the table is foldable and is initially
                                  //      unfolded and spans to the whole width
                                  //      of table holder
                  );

  // Make column headers (optional)
  rvapi_put_horz_theader ( "my_compact_table","Column 1","Tooltip 1",0 );
  rvapi_put_horz_theader ( "my_compact_table","Column 2","",1 );
  rvapi_put_horz_theader ( "my_compact_table","Column 3","Tooltip 2",2 );
  rvapi_put_horz_theader ( "my_compact_table","Column 4","",3 );

  // Make row headers (optional)
  rvapi_put_vert_theader ( "my_compact_table","Row 1","Tooltip 1",0 );
  rvapi_put_vert_theader ( "my_compact_table","** Row 2 **","",1 );

  // Fill table body. Any strings may be put in, however, there are
  // interface functions for other types of data. Note that one
  // can leave empty cells and intersperse data types at will
  for (int trow=0;trow<2;trow++)
    for (int tcol=0;tcol<4;tcol++)  {
      char S[100];
      sprintf ( S,"%10.3g",float(trow+1)*float(tcol+1)/3.14159265 );
      rvapi_put_table_string ( "my_compact_table",S,trow,tcol );
    }

  // now make expansion on the right from the table in order to make the
  // table compact
  rvapi_set_cell_stretch ( "my_grid", // grid reference
                            100,      // force horizontal expansion
                            0,        // ignore vertical dimension
                            0,        // row
                            1         // column
                          );

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();

  return 0;

}
