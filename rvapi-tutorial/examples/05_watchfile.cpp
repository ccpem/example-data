
/* ================================================================ .
 *                                                                  *
 *  EXAMPLE 05:  Tabbed HTML layout: file watch                     *
 *                                                                  *
 ' ================================================================ */

#include <unistd.h>
#include <sys/stat.h> // mkdir
#include <strings.h>
#include <stdio.h>

#include "../rvapi/src/rvapi_interface.h"


int main ( int argc, char ** argv, char ** env )  {
int    delay = 2;  // seconds, to imitate a runnning process
FILE  *f;
int    i;

  // create report directory just in case
  mkdir ( "report",0777 );

  // clear log file in case of repeat use
  remove ( "report/output.log" );

  // initialise document first
  rvapi_init_document ( "demo",               // mandatory, use any name
                        "./report",           // mandatory, use any existing output directory name
                        "Watched File in HTML output",  // mandatory, use any page title
                        RVAPI_MODE_Html,      // mandatory, use RVAPI_MODE_Html
                        RVAPI_LAYOUT_Tabs,    // mandatory, request tabbed layout
                        "../../rvapi/jsrview", // needed, either a URL, or absolute
                                               // path or path relative to
                                               // "./report"
                        NULL,                 // help file not applicable to basic layout
                        "index.html",         // may be NULL, then "index.html" is used
                        NULL,                 // default task file name (task.tsk)
                        NULL                  // special use for CCP4i2, ignore
                     );
  rvapi_flush();  // generate empty index.html at once

  rvapi_add_tab ( "tab1",   // tab reference Id
                  "Tab 1",  // tab name
                  true      // open at time of creation
                );

  // put tab text
  rvapi_set_text    ( "<h2>Content of Tab 1 follows here</h2>",  // text
                      "tab1",               // tab reference
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( 3*delay );  // give 5 secs to manually reload page in browser
  rvapi_flush();

  rvapi_set_text    ( "The tab displays the content of changing file",  // text
                      "tab1",               // tab reference
                      1,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();

  rvapi_add_content ( "output.log",    // URI to loaded content: either an URL,
                                       //   or absolute file path, or path
                                       //   relative to directory containing
                                       //   index.html
                      true,            // do watch
                      "tab1",          // tab reference
                      2,               // grid row,
                      0,               // grid column
                      1,               // row span,
                      1                // column span
                    );

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();

  // imitate dynamically created content in disk file, which will be
  // displayed in the tab
  f = fopen ( "report/output.log","w" );

  fprintf ( f,"%s\n","------------------------------------------------------" );
  fprintf ( f,"%s\n","LOG FILE BEGINS" );
  fflush  ( f );

  for (i=1;i<=10;i++)  {
    fprintf ( f," ... record number %i\n",i );
    fflush  ( f );
    sleep ( delay );  // imitate delay due to a running process
  }

  fprintf ( f,"%s\n","LOG FILE ENDS" );
  fprintf ( f,"%s\n","------------------------------------------------------" );
  fclose ( f );

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();


  return 0;

}
