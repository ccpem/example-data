
/* ================================================================ .
 *                                                                  *
 *  EXAMPLE 08:  Plain HTML layout: Radar                            *
 *                                                                  *
 ' ================================================================ */

#include <unistd.h>
#include <sys/stat.h> // mkdir

#include "../rvapi/src/rvapi_interface.h"


int main ( int argc, char ** argv, char ** env )  {
int   delay = 2;  // seconds, to imitate a runnning process

  // create report directory just in case
  mkdir ( "report",0777 );

  // initialise document first
  rvapi_init_document ( "demo",               // mandatory, use any name
                        "./report",           // mandatory, use any existing output directory name
                        "Plain HTML Layout: Radar",  // mandatory, use any page title
                        RVAPI_MODE_Html,      // mandatory, use RVAPI_MODE_Html
                        RVAPI_LAYOUT_Plain,   // mandatory, request plain page
                        "../../rvapi/jsrview", // needed, either a URL, or absolute
                                               // path or path relative to
                                               // "./report"
                        NULL,                 // help file not applicable to basic layout
                        "index.html",         // may be NULL, then "index.html" is used
                        NULL,                 // default task file name (task.tsk)
                        NULL                  // special use for CCP4i2, ignore
                     );
  rvapi_flush();  // generate empty index.html at once

  // put title
  rvapi_set_text    ( "<h2>PLAIN HTML PAGE OUTPUT: RADAR</h2>",  // title
                      "body",               // root grid for plain page layout
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( 3*delay );  // give 5 secs to manually reload page in browser
  rvapi_flush();

  // Add radar widget
  rvapi_add_radar ( "radar",          // radar widget reference (Id)
                    "Radar example",  // radar title
                    "body",           // radar holder reference
                    1,                // holder row
                    0,                // holder column
                    1,                // holder row span
                    1,                // holder column span
                    0   // radar widget fold state:
                        //   0: the widget is not foldable
                        //  -1: the widget is foldable and is initially folded
                        //   1: the widget is foldable and is initially unfolded
                  );

  // Set (a minimum of 3) radar properties and their values
  rvapi_add_radar_property ( "radar",  // radar widget reference
                             "Area",   // property name
                             0.1       // property value
                           );
  rvapi_add_radar_property ( "radar","Energy"    ,0.5 );
  rvapi_add_radar_property ( "radar","Velocity"  ,0.3 );

  // Now add new properties, 1 per set delay interval

  rvapi_flush();
  sleep ( delay );

  rvapi_add_radar_property ( "radar","Mass"      ,0.9 );

  rvapi_flush();
  sleep ( delay );

  rvapi_add_radar_property ( "radar","Brightness",1.0 );

  rvapi_flush();
  sleep ( delay );

  rvapi_add_radar_property ( "radar","Volume"    ,0.7 );

  return 0;

}
