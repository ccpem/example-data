
/* ================================================================ .
 *                                                                  *
 *  EXAMPLE 04:  Tabbed HTML layout                                 *
 *                                                                  *
 ' ================================================================ */

#include <unistd.h>
#include <sys/stat.h> // mkdir
#include <stdio.h>

#include "../rvapi/src/rvapi_interface.h"


int main ( int argc, char ** argv, char ** env )  {
int   delay = 2;  // seconds, to imitate a runnning process

  // create report directory just in case
  mkdir ( "report",0777 );

  // initialise document first
  rvapi_init_document ( "demo",               // mandatory, use any name
                        "./report",           // mandatory, use any existing output directory name
                        "Tabbed HTML Layout", // mandatory, use any page title
                        RVAPI_MODE_Html,      // mandatory, use RVAPI_MODE_Html
                        RVAPI_LAYOUT_Tabs,    // mandatory, request tabbed layout
                        "../../rvapi/jsrview", // needed, either a URL, or absolute
                                               // path or path relative to
                                               // "./report"
                        NULL,                 // help file not applicable to basic layout
                        "index.html",         // may be NULL, then "index.html" is used
                        NULL,                 // default task file name (task.tsk)
                        NULL                  // special use for CCP4i2, ignore
                     );
  rvapi_flush();  // generate empty index.html at once

  rvapi_add_tab ( "tab1",   // tab reference Id
                  "Tab 1",  // tab name
                  true      // open at time of creation
                );

  // put tab text
  rvapi_set_text    ( "<h2>Content of Tab 1 follows here</h2>",  // text
                      "tab1",               // tab reference
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( 3*delay );  // give 5 secs to manually reload page in browser
  rvapi_flush();


  rvapi_add_tab ( "tab2",   // tab reference Id
                  "Tab 2",  // tab name
                  true      // open at time of creation
                );

  // put tab text
  rvapi_set_text    ( "<h2>Content of Tab 2 follows here</h2>",  // text
                      "tab2",               // tab reference
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();


  rvapi_add_tab ( "tab3",   // tab reference Id
                  "Tab 3",  // tab name
                  true      // open at time of creation
                );

  // put tab text
  rvapi_set_text    ( "<h2>Content of Tab 3 follows here</h2>",  // text
                      "tab3",               // tab reference
                      0,                    // grid row,
                      0,                    // grid column
                      1,                    // row span,
                      1                     // column span
                    );

  sleep ( delay );  // imitate delay due to a running process
  rvapi_flush();


  return 0;

}
