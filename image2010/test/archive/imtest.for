c******************************************************************
C*** imtest - program to test new imsubs2000		23.02.00
C******************************************************************
C***	1. Open/Read old style file imtest1.map
C***	2. return all parameters
C***	3. Write new style file imtest2.map, adding a title
C***    4. close, reopen imtest2.map, open 2nd new file imtest3.map
C***	5. return all parameters
C***	6. Modify parameters with symmetry operators, rewrite header imtest2.map
C***    7. write 2nd new style file imtest3.map
C***	8. close/open/Read new style file imtest3.map
C***    9. read part of a section, write out, read whole section
C***   10. open imtest4.map, transfer header data, mode 2
C***   11. write part of a section to imtest4.map
C***
	integer*4	idevin
	parameter	(idevin = 1)
	integer*4	idevout
	parameter	(idevout = 2)
	integer*4	nlabels
	parameter	(nlabels = 0)
	integer*4	maxsiz
	parameter	(maxsiz = 512)
C***
	character	filename*512
	character	title*80
	character	titles(20)*80
C***
	integer*4	jxyz(3)
	integer*4	kxyz(3)
	integer*4	lxyz(3)
	integer*4	nxyz(3)
	integer*4	nxyzst(3)
	integer*4	mxyz(3)
	integer*4	iuvw(3)
C***
	real*4		aline(maxsiz)
	real*4		array(maxsiz,maxsiz)
	real*4		brray(31,31)
	real*4		cell(6)
	real*4		extra(25)
	real*4		labels(20,10)
	real*4		origin(3)
	real*4		section(maxsiz*maxsiz)
C***
	equivalence	(array(1,1),section(1))
	equivalence	(labels(1,1),titles(1)(1:1))
	istart = 1
	nextra = 23
C*********************************************************************
C*** copy from original file
C*********************************************************************
	write(6,'(''******** Saving starting test file...''/)')
	call system('cp imtest.map.save imtest.map')
C*********************************************************************
C*** 1. open test file/read header and data
C*********************************************************************
	write(6,'(''******** Opening test file and reading header...''/)')
	call imopen(idevin,'imtest.map','old')
	call irdhdr(idevin,nxyz,mxyz,mode,dmin,dmax,dmean)
	nx = nxyz(1)
	ny = nxyz(2)
	do iy=1,ny
	 call irdlin(idevin,aline)
	 do ix=1,nx
	  array(ix,iy) = aline(ix)
	 end do
	end do
C*********************************************************************
C*** 2. return values from header
C*********************************************************************
	write(6,'(''******** Returning values from header ...''/)')
	call irtcel(idevin,cell)
	write(6,'(''irtcel Cell axes ....................... '',3g11.4)')
     *  (cell(j),j=1,3)
	write(6,'(''irtcel Cell angles ..................... '',3f9.3)')
     *  (cell(j),j=4,6)
	call irtext(idevin,extra,istart,nextra)
	write(6,'(''irtext Extra ........................... ''/25a4)')
     *  (extra(j),j=1,25)
	call irtlab(idevin,labels,nl)
	write(6,'(''irtlab Labels .......................... ''/10(1x,20a4/))')
     *  ((labels(i,k),i=1,20),k=1,nl)
	call irtorg(idevin,xorigin,yorigin,zorigin)
	write(6,'(''irtorg Origin .......................... '',3g13.5)')
     *  origin
	call irtsam(idevin,mxyz)
	write(6,'(''irtsam mxyz ............................ '',3i6)')
     *  mxyz
	call irtsiz(idevin,nxyz,mxyz,nxyzst)
	write(6,'(''irtsiz nxyz,mxyz,nxyzst................. '',9i6)')
     *  nxyz,mxyz,nxyzst
	call irtsym(idevin,kspg,kbs)
	write(6,'(''irtsym space grp, bytes symmetry ....... '',2i6)')
     *  kspg,kbs
	call irtuvw(idevin,iuvw)
	write(6,'(''irtuvw iuvw ............................ '',3i6)')
     *  iuvw
C*******************************************************************
C*** 3. open new file, transfer data from old to new map
C*******************************************************************
	write(6,'(/''******** Opening new output file imtest2.map....'')')
	call imopen(idevout,'imtest2.map','unknown')
	nl = 1
	call icrhdr(idevout,nxyz,mxyz,mode,labels,nl)
	call itrlab(idevout,idevin)
	dmin = 1000000.
	dmax = -dmin
	dmean = 0.
C*** write data
	do iy=1,ny
	 do ix=1,nx
	  aline(ix) = array(ix,iy)
	  den = aline(ix)
	  dmin = min(dmin,den)
	  dmax = max(dmax,den)
	  dmean = dmean + den
	 end do
	 call iwrlin(idevout,aline)
	end do
	dmean = dmean / float(nx * ny)
	ntflag = 1
	nl = nl + 1
	title = 'New imtest file : imtest2.map'
	write(6,'(''Write header'')')
	call iwrhdr(idevout,title,ntflag,dmin,dmax,dmean)
	write(6,'(''read header'')')
	call irdhdr(idevout,nxyz,mxyz,mode,dmin,dmax,dmean)
C*********************************************************************
C*** 4. Close both files, reopen imtest2.map
C*********************************************************************
	write(6,'(/''******** Closing both files, reopen imtest2.map...'')')
	call imclose(idevin)
	call imclose(idevout)
	call imopen(idevin,'imtest2.map','old')
	call irdhdr(idevin,nxyz,mxyz,mode,dmin,dmax,dmean)
	call imopen(idevout,'imtest3.map','unknown')
C*********************************************************************
C*** 5. Return all parameters
C*********************************************************************
	write(6,'(''******** Returning values from imtest2.map ...''/)')
	call irtcel(idevin,cell)
	write(6,'(''irtcel Cell axes ....................... '',3g11.4)')
     *  (cell(j),j=1,3)
	write(6,'(''irtcel Cell angles ..................... '',3f9.3)')
     *  (cell(j),j=4,6)
	call irtext(idevin,extra,istart,nextra)
	write(6,'(''irtext Extra ........................... ''/25a4)')
     *  (extra(j),j=1,25)
	call irtlab(idevin,labels,nl)
	write(6,'(''irtlab Labels .......................... ''/10(1x,20a4/))')
     *  ((labels(i,k),i=1,20),k=1,nl)
	call irtorg(idevin,xorigin,yorigin,zorigin)
	write(6,'(''irtorg Origin .......................... '',3g13.5)')
     *  origin
	call irtsam(idevin,mxyz)
	write(6,'(''irtsam mxyz ............................ '',3i6)')
     *  mxyz
	call irtsiz(idevin,nxyz,mxyz,nxyzst)
	write(6,'(''irtsiz nxyz,mxyz,nxyzst................. '',9i6)')
     *  nxyz,mxyz,nxyzst
	call irtsym(idevin,kspg,kbs)
	write(6,'(''irtsym space grp, bytes symmetry ....... '',2i6)')
     *  kspg,kbs
	call irtuvw(idevin,iuvw)
	write(6,'(''irtuvw iuvw ............................ '',3i6)')
     *  iuvw
C*********************************************************************
C*** 6. modify values in header, add symmetry operators
C*********************************************************************
	write(6,'(/''******** Modifying header values for imtest2.map. ''/)')
	do i=1,2
	 jxyz(i) = nxyz(i) * 2
	 cell(i) = cell(i) * 2.
	 origin(i) = float(nxyz(i)) * 0.5
	 mxyz(i) = jxyz(i)
	end do
	jxyz(3) = 1
	origin(3) = 0.
	xorigin = origin(1)
	yorigin = origin(2)
	zorigin = origin(3)
	mxyz(3) = 1
	jx = jxyz(1)
	jy = jxyz(2)
C*** read data
	do iy=1,jy
	 do ix=1,jx
	  array(ix,iy) = 0.
	 end do
	end do
	do iy=1,ny
	 call irdlin(idevin,aline)
	 do ix=1,nx
	  array(ix,iy) = aline(ix)
	 end do
	end do
C***
	kspg = 1
	kbs = 80
	extra(1) = 999
	write(extra(2),'(''TEST'')')
	nl = 2
	titles(1)(1:80) = 'New test map imtest2.map'
	write(6,'(''Changing cell axes to .................. '',3g11.4)')
     *  (cell(j),j=1,3)
	write(6,'(''Keeping Cell angles .................... '',3f9.3)')
     *  (cell(j),j=4,6)
	call ialcel(idevin,cell)
	write(6,'(''Changing Extra ......................... ''/(5a4/))')
     *  (extra(j),j=1,25)
	call ialext(idevin,extra,istart,nextra)
	write(6,'(''Changing Labels ........................ ''/10(1x,20a4/))')
     *  ((labels(i,k),i=1,20),k=1,nl)
	call iallab(idevin,labels,nl)
	write(6,'(''Changing Origin ........................ '',3g13.5)')
     *  origin
	call ialorg(idevin,xorigin,yorigin,zorigin)
	write(6,'(''Changing mxyz .......................... '',3i6)')
     *  mxyz
	call ialsam(idevin,mxyz)
	write(6,'(''Changing nxyz,nxyzst............... '',6i6)')
     *  nxyz,nxyzst
	call ialsiz(idevin,jxyz,nxyzst)
	write(6,'(''Changing space grp, bytes symmetry ..... '',2i6)')
     *  kspg,kbs
	call ialsym(idevin,kspg,kbs)
	write(6,'(''Keeping iuvw ........................... '',3i6)')
     *  iuvw
	ntflag = -1
	dmin = 0.
	dmax = 0.
	dmean = 0.
c	call iwrhdr(idevin,title,ntflag,dmin,dmax,dmean)
C*** rewrite data
	call imposn(idevin,0,0)
	nlines = 0
C*** initialize o/p array
	do ix=1,jx
	 aline(ix) = 0.
	end do
	do iy=1,ny
	 do ix=1,nx
	  aline(ix) = array(ix,iy)
	  den = aline(ix)
	  dmin = min(dmin,den)
	  dmax = max(dmax,den)
	  dmean = dmean + den
	 end do
	 nlines = nlines + 1
	 call iwrlin(idevin,aline)
	end do
C*** initialize again
	do ix=1,jx
	 aline(ix) = 10.
	end do
	do iy=1,jy-ny
	 nlines = nlines + 1
	 call iwrlin(idevin,aline)
	end do
	write(6,'('' nlines '',i5)')nlines
	ntflag = -1
	dmean = dmean / float(jx * jy)
	call iwrhdr(idevin,title,ntflag,dmin,dmax,dmean)
C*** read header back again
	call irdhdr(idevin,jxyz,mxyz,mode,dmin,dmax,dmean)
C***************************************************************
C*** 7. write file to last map imtest3.map
C***************************************************************
	write(6,'(/''******** copy to file imtest3.map'')')
	nl = 1
	mode3 = 1
	call imopen(idevout,'imtest3.map','unknown')
	call itrhdr(idevout,idevin)
	call ialmod(idevout,mode3)
	dmin = 1000000.
	dmax = -dmin
	dmean = 0.
	call imposn(idevin,0,0)
C*** write data
	do iy=1,jy
	 call irdlin(idevin,aline)
	 do ix=1,jx
	  den = aline(ix)
	  dmin = min(dmin,den)
	  dmax = max(dmax,den)
	  dmean = dmean + den
	 end do
	 call iwrlin(idevout,aline)
	end do
	dmean = dmean / float(jx * jy)
	ntflag = 1
	nl = nl + 1
	title = '2nd New imtest file : imtest3.map'
	titles(2)(1:80) = title
	call iwrhdr(idevout,title,ntflag,dmin,dmax,dmean)
C***********************************************************************
C*** 8. close/reopen/read imtest3.map
C***********************************************************************
	write(6,'(/''******** close, reopen read imtest3.map ....'')')
	call imclose(idevin)
	call imclose(idevout)
	call imopen(idevin,'imtest3.map','old')
	call irdhdr(idevin,nxyz,mxyz,mode,dmin,dmax,dmean)
C*** return values from header
	write(6,'(/''Returned values from imtest3.map ...'')')
	call irtcel(idevin,cell)
	write(6,'(''irtcel Cell axes ....................... '',3g11.4)')
     *  (cell(j),j=1,3)
	write(6,'(''irtcel Cell angles ..................... '',3f9.3)')
     *  (cell(j),j=4,6)
	call irtext(idevin,extra,istart,nextra)
	write(6,'(''irtext Extra ........................... ''/25a4)')
     *  (extra(j),j=1,25)
	call irtlab(idevin,labels,nl)
	write(6,'(''irtlab Labels .......................... ''/10(1x,20a4/))')
     *  ((labels(i,k),i=1,20),k=1,nl)
	call irtorg(idevin,xorigin,yorigin,zorigin)
	write(6,'(''irtorg Origin .......................... '',3g13.5)')
     *  origin
	call irtsam(idevin,mxyz)
	write(6,'(''irtsam mxyz ............................ '',3i6)')
     *  mxyz
	call irtsiz(idevin,nxyz,mxyz,nxyzst)
	write(6,'(''irtsiz nxyz,mxyz,nxyzst................. '',9i6)')
     *  nxyz,mxyz,nxyzst
	call irtsym(idevin,kspg,kbs)
	write(6,'(''irtsym space grp, bytes symmetry ....... '',2i6)')
     *  kspg,kbs
	call irtuvw(idevin,iuvw)
	write(6,'(''irtuvw iuvw ............................ '',3i6)')
     *  iuvw
C************************************************************************
C*** 9. read part of a section
C************************************************************************
	call irdpas(idevin,brray,31,31,19,49,29,59,*900)
	do i=1,31
	 write(6,'(12f6.1)') (brray(j,i),j=1,12)
	end do
	call imposn(idevin,0,0)
	call irdsec(idevin,section,*900)
C************************************************************************
C***   10. open imtest4.map, transfer header data, mode 2
C************************************************************************
	lxyz(1) = 11
	lxyz(2) = 11
	lxyz(3) = 1
	do i=1,3
	 kxyz(i) = lxyz(i)
	end do
	mode4 = 2
	nl = 3
	call imopen(idevout,'imtest4.map','unknown')
	call icrhdr(idevout,lxyz,kxyz,mode4,labels,nl)
	call itrlab(idevout,idevin)
	titles(3)(1:80) = '3rd New imtest file : imtest4.map'
	call iallab(idevout,labels,nl)
	call ialmod(idevout,mode4)
	call iwrpas(idevout,brray,31,31,9,19,4,14)
	do i=5,15
	 write(6,'(11f6.1)') (brray(j,i),j=10,20)
	end do
	dmin = 1000000.
	dmax = -dmin
	dmean = 0.
	do iy=5,15
	 do ix=10,20
	  den = brray(ix,iy)
	  dmin = min(dmin,den)
	  dmax = max(dmax,den)
	  dmean = dmean + den
	 end do
	end do
	dmean = dmean / 121.
C************************************************************************
C***   11. write part of a section to imtest4.map
C************************************************************************
	ntflag = -1
	call iwrhdr(idevout,title,ntflag,dmin,dmax,dmean)
	call system('ls -l imtest.map.save')
	call system('ls -l imtest2.map')
	call system('ls -l imtest3.map')
	call system('ls -l imtest4.map')
	stop
900     write(6,'(''error reading data'')')
	end
