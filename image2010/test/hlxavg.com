#!/bin/csh -f
#
set progs  = $IMAGEBIN
/bin/rm -f hlxavg.nr
/bin/rm -f LLPLOT.PLT
setenv LLOUT hlxavg.nr
$progs/mrc_hlxavg << 'eot'
Average hlx1 and hlx2
7,2,320.,0.0004,0
hlx1.nr
0,0,0.,0.,1.
1.,1.,1.,1.,1.,1.,1.
hlx2.nr
0,0,-15.,0.,1.
1.,1.,1.,1.,1.,1.,1.
'eot'
