#!/bin/csh -x
#
#
set progs=$IMAGEBIN
setenv IN  allspace.data
time $progs/mrc_allspacea << eot
ALL 
T T T 4000				! SEARCH,REFINE,TILT,NCYC
0. 0. 0. 0.				! ORIGH,ORIGK,TILTH,TILTK
6.0  61					! STEPSIZE, PHASE SEARCH ARRAY SIZE
62.45 62.45 120.0   150 3.5   2.0 120	! A, B, GAMMA, RIN, ROUT, CS, KV
F 0 7					! ILIST,ROT180,IQMAX
eot
#
#102.0 102.0 90.0   150 3.5   2.0 120	! HR A, B, GAMMA, RIN, ROUT, CS, KV
#
