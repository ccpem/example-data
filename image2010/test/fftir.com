#!/bin/csh -x
#
#                       need to change only parameters that follow.
set image   = "h123"
set cutoffs = "40 250"
set progs = $IMAGEBIN
/bin/rm -f ${image}.cut
/bin/rm -f ${image}.fft
/bin/rm -f ${image}.int
/bin/rm -f ${image}.tap
cd $IMAGETEST
#
#  FFTIMGRED - taperedge, cutoff, and initial pixel averaging and FFT.
#     taperedge is needed for spotscan images to eliminate transform stripes.
#     cutoffs help to reduce the effects of bits of dust and hairs.
#
setenv IN  ${image}.pic
setenv OUT ${image}.tap
time ${progs}/mrc_taperedgek  << 'eot'
20,20,200	! IAVER,ISMOOTH,ITAPER
20,20,200	! JAVER,JSMOOTH,JTAPER
'eot'
/bin/rm -f YPLOT.PS
#
${progs}/mrc_label << eot
${image}.tap
99		! other options
3		! cutoff over and underflows
${image}.cut
${cutoffs}
eot
#
#/bin/rm -f ${image}.tap
${progs}/mrc_label << eot
${image}.cut
4		! average adjacent pixels
${image}.red
2,2,1		! over 2X2 area
eot
setenv IN  ${image}.red
setenv OUT ${image}.fft
${progs}/mrc_fftrans
#
#
#
#  FFTPRELIM to allow the transform to be examined on graphics device.
#
${progs}/mrc_label << eot
${image}.fft
5		! convert to intensities
${image}.int
1
eot
#/bin/rm -f ${image}.red
${progs}/mrc_lasertone -lower=0.0 -upper=18 ${image}.int  << eot
0,499,180,820
eot
### PRINT tone.ps on a postscript printer
echo " Now laserprint the postscript file tone.ps "
#lpr -Plaser1 tone.ps
