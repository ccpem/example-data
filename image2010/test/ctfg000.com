#!/bin/csh -x
#
set progs = ${IMAGEBIN}
#
set defocus = "5500 6600 20"
set title   = " pass 2, boxed"
set date    = "15.2.94"
set image   = "g000"
set filmno  = "1000"
set cell    = "33.86 26.30 26.65 -33.99"
set xmag    = "34300"
#
#   CTFAPPLY 
#
# Next line commented out for Darwin (Mac OS/X).  See last line instead
#setenv CTFPLOT ctf${image}.ps
setenv IN  raw${image}.aph
setenv OUT ctf${image}.aph
#
time ${progs}/mrc_ctfapplyk << eot
${cell}  2000 7.5 ${xmag}		! AX,AY,BX,BY,ISIZE,DSTEP,XMAG
${defocus} 2.0 120			! DFMID1,DFMID2,ANGAST,CS,KV
      ${filmno} CTF${image} defocus=${defocus}, ${date} - ${title}
eot
mv CTFPLOT ctf${image}.ps
#lpr -Plaser1 ctf${image}.ps
