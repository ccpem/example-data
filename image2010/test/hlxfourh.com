#!/bin/csh -f
#
set progs  = $IMAGEBIN
/bin/rm -f helix_av.map
/bin/rm helix_proj.map
setenv LLIN hlxavg.nr
setenv OUT1 helix_av.map
setenv OUT2 helix_proj.map
setenv GOUT /dev/null
$progs/mrc_hlxfour << 'eot'
Average of hlx1,hlx2 14Dec95
320.,.0004,180.,5.
7,0,2
1.,1.,1.,1.,1.,1.,1.
1 0 0 0 0 0 0 0
0. 360.
0. 320. 5.
'eot'
