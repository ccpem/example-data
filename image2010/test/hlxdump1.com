#!/bin/csh -f
#
set progs = $IMAGEBIN
/bin/rm -f LLPLOT.PLT
setenv IN hlx1.trn
setenv LLDAT1 hlx1.nr
setenv LLDAT2 hlx1.fr
time $progs/mrc_hlxdump << 'eot'
Helix 1 dump 9Dec95
7 5. 0. 0. 0.0004 0. 0. 0.
-0.05 0.05 1 0 0
0 0 0.
0 55 0 55
1 8 8.
7 33 7 33
2 -7 16.
7 37 7 37
3 1 24.
0 51 0 51
4 9 32.
10 35 10 35
5 -6 40.
6 44 6 44
6 2 48.
0 37 0 37
'eot'
