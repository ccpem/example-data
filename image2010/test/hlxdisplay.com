#!/bin/csh -f
#
echo "running surf"
/bin/rm -f helix.sur
setenv IN helix_av.map
setenv OUT helix.sur
$progs/mrc_surf << 'eot'
0.,0.,10,2
'eot'
#
echo "running light"
/bin/rm -f helix.lig
setenv IN helix.sur
setenv OUT helix.lig
$progs/mrc_light << 'eot'
0,0,0.25,0.75,6,0.
'eot'
#
echo "running Ximdisp"
$progs/mrc_Ximdisp -f Ximdisp.def
