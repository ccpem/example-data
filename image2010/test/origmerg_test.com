#!/bin/csh -x
date
#
#  Test run of ORIGTILTK for simple test of compilers
#
set progs  = $IMAGEBIN
set rundate = ` date | awk ' {print $3,$2,$6} ' `
#
#
setenv   PLOT  plot7014_test.ps
setenv   SUMMARY  summary_merg7014_test.dat
setenv   FORT3    merg7014_3d_test.aph
#
time $progs/mrc_origtiltk << eot
12 0 F F 0 102. 102. 100. 90 1 8 !ISPG,NPRG,NTL,NBM,ILST,A,B,W,ANG,IPL,MNRF
7014 0 0 5 3 F F		 !IRUN,LHMN,LHMX,IQMX,IBXPHS,NREFOUT,NSHFTIN
       600  dummy superlattice data
dummy.aph
      2464   Halorhodopsin superlattice in ice,  6.4.93
ctf2464.aph
  F
5.422 6.773 1		   		! TAXA,TANGL,IORIGT
-50.00 -22.00 0.0 0.010 0 1.37931 0 0 0      !OH,OK,STEP,WIN,SGNXCH,SCL,ROT,REV,CTFREV
2.0 120. 0.0 0.0			! cs,kv,tx,ty
200.0 7				! resolution limits
      3144   Halorhodopsin Berlin image in ice,  29-Apr-95
ctfvt3144.aph
  F
9.506 7.747 1		   		! TAXA,TANGL,IORIGT
-14.00 86.00 0.0 0.010 0 0 0 0 0      !OH,OK,STEP,WIN,SGNXCH,SCL,ROT,REV,CTFREV
1.35 160. 0.0 0.0			! cs,kv,tx,ty
200.0 5.5				! resolution limits
      3145   Halorhodopsin Berlin image in ice,  29-Apr-95
ctfvt3145.aph
  F
-3.453 -5.724 1		   		! TAXA,TANGL,IORIGT
-128.00 -193.00 0.0 0.010 0 0 0 0 0      !OH,OK,STEP,WIN,SGNXCH,SCL,ROT,REV,CTFREV
1.35 160. 0.0 0.0			! cs,kv,tx,ty
200.0 5.5				! resolution limits
      3165   Halorhodopsin Berlin image in ice,  29-Apr-95
ctfvt3165.aph
  F
97.806 4.347 1		   		! TAXA,TANGL,IORIGT
75.00 56.00 0.0 0.010 0 0 0 0 0      !OH,OK,STEP,WIN,SGNXCH,SCL,ROT,REV,CTFREV
1.35 160. 0.0 0.0			! cs,kv,tx,ty
200.0 5.5				! resolution limits
      3112   Halorhodopsin Berlin image in ice,  29-Apr-95
ctfvt3112.aph
  F
86.665 0.852 1		   		! TAXA,TANGL,IORIGT
67.00 73.00 0.0 0.010 0 0 0 0 0      !OH,OK,STEP,WIN,SGNXCH,SCL,ROT,REV,CTFREV
1.35 120. 0.0 0.0			! cs,kv,tx,ty
200.0 5.5				! resolution limits
      3114   Halorhodopsin Berlin image in ice,  29-Apr-95
ctfvt3114.aph
  F
49.279 -5.370 1		   		! TAXA,TANGL,IORIGT
-118.00 -22.00 0.0 0.010 0 0 0 0 0      !OH,OK,STEP,WIN,SGNXCH,SCL,ROT,REV,CTFREV
1.35 120. 0.0 0.0			! cs,kv,tx,ty
200.0 5.5				! resolution limits
        -1
 Test 6 HR images, 13.6.95 rundate ${rundate}
eot
\rm -f summary_merg7014_test.dat
date
