#!/bin/csh -x
#
#	DOPROF contains BACKAUTO,AUTOINDEX and PICKPROFA
#          - includes four laserprints, three plots and one tone
#
set progs  = $IMAGEBIN
set film   = "s730"
set number = " 730"
set centre = "-5  14"
#
date
#
#   LABEL to select 1150x1150 square area
\rm -f ${film}.box
time ${progs}/mrc_label << eot
${film}.pat
1
${film}.box
67,1216,1,1150
eot
#
#   LABEL to put data on same scale as film
\rm -f ${film}.div
time ${progs}/mrc_label << eot
${film}.box
2
${film}.div
0.025  -4.0
1
eot
#
#   delete old files to make sure that there is no problem later
\rm -f  ${film}.crt
\rm -f  ${film}.bck
\rm -f  ${film}.ycr
\rm -f  ${film}b.ps
#
# BACKAUTO automatic radial background calculation
#
setenv IN       ${film}.div
setenv OUT      ${film}.crt
setenv FORT2    ${film}.bck
setenv FORT4    ${film}.ycr
setenv PLOTBACK ${film}b.ps
${progs}/mrc_backautok << eot
T,T		! IREF,IPLOT
 ${centre}	! CX,CY centre of pattern if set by hand
 ${number}     	! NPLATE
 distortion-corrected CCD pattern ${film}
  20		! NPNTS
  600  130	! IRMAX, IRMIN
  750  300	! IRMAXC,IRMINC
eot
#
# AUTOINDEX 
#
\rm -f  ${film}a.ps
\rm -f  ${film}.avr
#
setenv IN	${film}.crt
setenv OUT	${film}.avr
setenv PLOTOUT.PS	${film}a.ps
${progs}/mrc_autoindexk
#
#
skipauto:
#
restart:
# PICKPROF automatic e.d. spot integration
#
#    delete old files to avoid confusion
\rm -f  ${film}.out
\rm -f  ${film}prof.ps
\rm -f  ${film}.lst
#

# Following test returns error:
# CCP4:   Open failed: File: s730.bck                                                                                                   
# CCP4:   Open failed: File: s730.bck                                                                                                   

#setenv IN      ${film}.div
#setenv TABLEIN ccd_distort_4dec97.dat
#setenv INPARAM ${film}.crt
#setenv DENOUT  ${film}.out
#setenv PLOTOUT.PS ${film}prof.ps
#setenv FORT2  ${film}.lst
#setenv RADIAL  ${film}.bck
#setenv YCORR  ${film}.ycr
##
#${progs}/mrc_pickprofk << eot
# 62.45,62.45,120.0,-44.0,0.999,1,120,F	! .....,IPLOT,KV,LPRINT
#T,F,7	! Use distortion table ? T/F Global background correction LGLOBL,NGLOBL
#T,2,85,F,T,0.03,2.0 ! LPROFIT,LPTYPE,LPRANGE,INTERLEAVE,IFLATTEN,PROFMIN,TSIGMA
# 1,1
# 48,48
# 150,150
# 1500,1500
# LOOK-UP TABLE FOR CCD, straight line, slope unity
#  ${number}
#  distortion-corrected CCD pattern ${film}
#F 0 0
#0 0 0 0 0		! CENTRE COORDS, TILTANGS, B3
#0 0 0 0			! LATTICE PARAMETERS
# 2.0, 40.0, 2.0			! RESOLUTION LIMITS
#0,0,0,0				! nxm,nym,  nxmt,nymt
# 1.0, 1500.0, 1.1, 6		! Fract,  Absol, Xamine, NCYC=0 (no refine)
#eot
##
