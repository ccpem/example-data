#!/bin/csh -x
#
# sample command script for a 2000x2000 image of a crystal
#   - need to change only params below for each image
#
# for a new project also need to change cell dimensions and resolution in MMBOX
#   radii in MASKTRAN, and possibly boxsizes in BOXIMAGE 
#
set date   = "24.8.95"
set title  = "HR ICE untilted "
set cell   = "28.73 12.76 13.03 -29.43"
set image  =  h123
set filmno = " 123"
set saved  = $IMAGETEST
set progs  = $IMAGEBIN
#
#/bin/rm -f ${image}.int
#
#  This script (JOBALL) is a concatenation of the VAX command files 
#     JOBA, CCUC, MMBOX, FFTCORRED, JOBB, CCUC, MMBOX
#  It must be run after FFTIMGRED, FFTPRELIM which are needed to create
#     the raw Fourier transform from which the list of visible spots and
#     the exact lattice parameters are obtained.  These are used 3 times and 
#     7 times in this script respectively - substitution is carried out
#     using the parameters ${cell} and ${image} and the file SPOTS
#  INPUT FILES required are
#                        image.pic  raw digitised image
#                    or   image.tap  taperedged image with spotscan recording
#                    or   image.cut  truncated to remove dirt
#                        image.fft  fft of the above, compressed 2x2
#                        SPOTS      list of visibly detectable spots in fft
#  
#
#  JOBA masks transform, calcs cross-correl map, and produces cross-correl
#           list of peaks
#
#   MASKTRAN - JOBA
setenv IN  ${image}.fft
setenv OUT ${image}.msk
setenv SPOTS ${saved}/${image}.spt
time ${progs}/mrc_masktrana << eot
1 T T	! ISHAPE= 1(CIRC),2(GAUSS CIRC),OR 3(RECT) HOLE, IAMPLIMIT(T or F)
4	! RADIUS OF HOLE IF CIRCULAR, X,Y HALF-EDGE-LENGTHS IF RECT.
${cell} -9 9 -9 9 200 1 !A/BX/Y,IH/IKMN/MX,RMAX,ITYPE
eot
#    FFTMSK - JOBA
setenv IN  ${image}.msk
setenv OUT ${image}.flt
time ${progs}/mrc_fftrans
#    LABEL FOR SMALL BOX IN CENTRE - JOBA
time ${progs}/mrc_label << eot
${image}.flt
1
box${image}.flt
487,512,487,512
eot
setenv IN  box${image}.flt
setenv OUT auto${image}.cor
time ${progs}/mrc_autocorrl << eot
20
eot
time ${progs}/mrc_label << eot
auto${image}.cor
1
auto${image}.map
210,310,210,310
eot
#/bin/rm -f box${image}.flt
#/bin/rm -f auto${image}.cor
#     BOXIMAGE - JOBA
setenv IN  ${image}.flt
setenv OUT ref${image}.flt
time ${progs}/mrc_boximage << eot
4	!  NOVERT, VERTEX COORDS GIVEN IN GRID UNITS RELATIVE TO (0,0) ORIGIN.
0 0		! ORIGIN FOR LATER USE (E.G. IN FOURIER TRANSFORM)
400 400 	! VERTEX COORDINATES IN GRID STEPS WRT CORNER (0,0)
600 400
600 600
400 600
eot
#/bin/rm -f ${image}.flt
#    FFTREF - JOBA
setenv IN  ref${image}.flt
setenv OUT ref${image}.fft
time ${progs}/mrc_fftrans
#/bin/rm -f ref${image}.flt
/bin/rm cor${image}.fft
#  TWOFILE - JOBA
#    Multiply two files together    :    FILE1 * Complex Conjugate of FILE2
#    ICOMB = 2
#        First image on stream 1 (IN1)
#        Second image on stream 2 (IN2)
#        Product output on stream 3 (OUT)
#
setenv IN1 ${image}.msk
setenv IN2 ref${image}.fft
setenv OUT cor${image}.fft
time ${progs}/mrc_twofile << eot
2		! ICOMB = 2
2 0 0 500 500	! IORIGIN,OXA,OYA,OXB,OYB  Origin shifts to FFT's.
eot
#/bin/rm -f ${image}.msk
#/bin/rm -f ref${image}.fft
#    FFTCOR - JOBA
setenv IN  cor${image}.fft
setenv OUT cor${image}.cor
time ${progs}/mrc_fftrans
#/bin/rm -f cor${image}.fft
#     QUADSERCHA - JOBA
setenv PROFILE  auto${image}.map
setenv PROFDATA prof${image}.dat
setenv CCPLOT quadserch1.ps
time ${progs}/mrc_quadserchk << eot
0,7				! IPASS,NRANGE
cor${image}.cor
1000,1000                         ! SIZE OF TRANSFORM (ISIZEX, ISIZEY)
${cell} F                  ! Lattice vectors
-35,35,-35,35			! NUMBER UNIT CELLS TO SEARCH
6,6				! RADIUS OF CORR SEARCH
500 500				! POSN OF START SEARCH ORIGIN  0,0 IS ORIGIN
N				! YES/NO FOR DETAILED PRINTOUT
28.0 28.0 45.0			! RADLIM IN PROFILE GRID UNITS
eot
#
#echo " laserprint postscript file "${image}"1.ps"
#lpr -Plaser1 ${image}1.ps
#
#    CCUNBEND - JOBA -- remember to change both titles on each run.
setenv CCORDATA prof${image}.dat
setenv CCPLOT ccunbend1.ps
time ${progs}/mrc_ccunbendk << eot
${image}.cut
0,1,60,25,F,40,F	!ITYPE,IOUT,IMAXCOR,ISTEP,LTAPER,RTAPER - essential except TAPER
30,52,0.001,0.13,46	!IKX,IKY,EPS,FACTOR,TLTAXIS -only FACTOR with nonbicubic
 ${title} SIMPLE UNBEND, ${date}, ${image} PASS 1
cor${image}.pic
 UNBENT  PASS 1, ccunbende, SIMPLE UNBEND ${date},  200x200 REFERENCE
eot
#lpr -Plaser1 ${image}C1.ps
setenv IN  cor${image}.pic
setenv OUT cor${image}.fft
time ${progs}/mrc_fftrans
#     MMBOX - JOBA
time ${progs}/mrc_mmboxa << eot
cor${image}.fft
     ${filmno}  ${title} ${date}, PASS 1
Y			! Use grid units?
Y			! Generate grid from lattice?
N			! Generate points from lattice?
2 1 0 26 26 15 15	! IPIXEL,IOUT,NUMSPOT,NOH,NOK,NHOR,NVERT
raw${image}.aph
1000. 1000.	! XORIG,YORIG
200.0 7.0 1 102. 102. 100.0 90.0 ! RINNER,ROUTER,IRAD,A,B,W,ABANG
${cell}			! Lattice vectors
eot
#
#
#     FFTCORRED
#
time ${progs}/mrc_label << eot
cor${image}.pic
4	! adjacent pixel averaging
cor${image}.red
2,2,1	! over 2X2 area
eot
setenv IN  cor${image}.red
setenv OUT cor${image}r.fft
time ${progs}/mrc_fftrans
##
#   MASKTRAN1 - JOBB- reference map 
setenv IN  cor${image}r.fft
setenv OUT ref${image}.msk
setenv SPOTS ${saved}/${image}.spt
time ${progs}/mrc_masktrana << eot
1 T T	! ISHAPE= 1(CIRC),2(GAUSS CIRC),OR 3(RECT) HOLE, IAMPLIMIT(T or F)
1	! RADIUS OF HOLE IF CIRCULAR, X,Y HALF-EDGE-LENGTHS IF RECT.
${cell} -9 9 -9 9 200 1 !A/BX/Y,IH/IKMN/MX,RMAX,ITYPE
eot
#/bin/rm -f cor${image}r.fft
#    FFTMSK - FFTRANS1 - JOBB
setenv IN  ref${image}.msk
setenv OUT ref${image}.flt
time ${progs}/mrc_fftrans
#/bin/rm -f ref${image}.msk
#     LABEL FOR SMALL BOX IN CENTRE - JOBB
time ${progs}/mrc_label << eot
ref${image}.flt
1
box${image}.flt
487,512,487,512
eot
setenv IN  box${image}.flt
setenv OUT auto${image}.cor
time ${progs}/mrc_autocorrl << eot
20
eot
time ${progs}/mrc_label << eot
auto${image}.cor
1
auto${image}.map
210,310,210,310
eot
#/bin/rm -f box${image}.flt
#/bin/rm -f auto${image}.cor
#    BOXIMAGE - JOBB
setenv IN  ref${image}.flt
setenv OUT refb${image}.flt
time ${progs}/mrc_boximage << eot
4	!  NOVERT, VERTEX COORDS GIVEN IN GRID UNITS RELATIVE TO (0,0) ORIGIN.
0 0		! ORIGIN FOR LATER USE (E.G. IN FOURIER TRANSFORM)
425 425	! VERTEX COORDINATES IN GRID STEPS WRT CORNER (0,0)
575 425
575 575
425 575
eot
#/bin/rm -f ref${image}.flt
#   FFTREF - FFTRANS2 - JOBB
setenv IN  refb${image}.flt
setenv OUT ref${image}.fft
time ${progs}/mrc_fftrans
#/bin/rm -f refb${image}.flt
#   MASKTRAN2:
#   MASKTRAN2 - original image to be searched - JOBB
setenv IN  ${image}.fft
setenv OUT ${image}.msk
setenv SPOTS ${saved}/${image}.spt
time ${progs}/mrc_masktrana << eot
1 T T	! ISHAPE= 1(CIRC),2(GAUSS CIRC),OR 3(RECT) HOLE, IAMPLIMIT(T or F)
6	! RADIUS OF HOLE IF CIRCULAR, X,Y HALF-EDGE-LENGTHS IF RECT.
${cell} -9 9 -9 9 200 1 !A/BX/Y,IH/IKMN/MX,RMAX,ITYPE
eot
#  TWOFILE - JOBB
#  Multiply two files together    :    FILE1 * Complex Conjugate of FILE2
#  ICOMB = 2
#        First image on stream 1 (IN1)
#        Second image on stream 2 (IN2)
#        Product output on stream 3 (OUT)
#
setenv IN1 ${image}.msk
setenv IN2 ref${image}.fft
setenv OUT corel${image}.fft
time ${progs}/mrc_twofile << eot
2		! ICOMB = 2
2 0 0 500 500	! IORIGIN,OXA,OYA,OXB,OYB  Origin shifts to FFT's.
eot
#/bin/rm -f ${image}.msk
#/bin/rm -f ref${image}.fft
#    FFTCOR - FFTRANS3 - JOBB
setenv IN  corel${image}.fft
setenv OUT cor${image}.cor
time ${progs}/mrc_fftrans
#/bin/rm -f corel${image}.fft
#       QUADSERCHA - JOBB
setenv PROFILE  auto${image}.map
setenv PROFDATA prof${image}.dat
setenv CCPLOT quadserch2.ps
time ${progs}/mrc_quadserchk << eot
0,6				! IPASS,NRANGE
cor${image}.cor
1000,1000                         ! SIZE OF TRANSFORM (ISIZEX, ISIZEY)
${cell} F                  ! Lattice vectors
-35,35,-35,35			! NUMBER UNIT CELLS TO SEARCH
6,6				! RADIUS OF CORR SEARCH
500 500				! POSN OF START SEARCH ORIGIN  0,0 IS ORIGIN
N				! YES/NO FOR DETAILED PRINTOUT
28.0 28.0 45.0			! RADLIM IN PROFILE GRID UNITS
eot
#echo "laserprint postscript file " ${image}2.ps
#lpr -Plaser1 ${image}2.ps
#/bin/rm -f auto${image}.map
#/bin/rm -f cor${image}.cor
#    CCUNBEND  - JOBB -- remember to change both titles on each run.
setenv CCORDATA prof${image}.dat
setenv CCPLOT ccunbend2.ps
time ${progs}/mrc_ccunbendk << eot
${image}.cut
0,1,60,25,F,40,F	!ITYPE,IOUT,IMAXCOR,ISTEP,LTAPER,RTAPER - essential except TAPER
30,52,0.001,0.13,46	!IKX,IKY,EPS,FACTOR,TLTAXIS -only FACTOR with nonbicubic
 ${title} SIMPLE UNBEND, ${date}, ${image} PASS 2
cor${image}.pic
 UNBENT  PASS 2, ccunbende, SIMPLE UNBEND ${date},  150x150 REFERENCE
eot
#/bin/rm -f cor${image}.fft
setenv IN  cor${image}.pic
setenv OUT cor${image}.fft
time ${progs}/mrc_fftrans
#
#      MMBOX - JOBB
#
time ${progs}/mrc_mmboxa << eot
cor${image}.fft
     ${filmno}  ${title} ${date}, PASS 2
Y			! Use grid units?
Y			! Generate grid from lattice?
N			! Generate points from lattice?
2 1 0 26 26 15 15	! IPIXEL,IOUT,NUMSPOT,NOH,NOK,NHOR,NVERT
raw${image}.aph
1000. 1000.	! XORIG,YORIG
200.0 7.0 1 102. 102. 100.0 90.0 ! RINNER,ROUTER,IRAD,A,B,W,ABANG
${cell}			! Lattice vectors
eot
#/bin/rm -f cor${image}.fft
#  leave behind only cor${image}.pic + prof${image}.dat
#
#lpr -Plaser1 ${image}C2.ps
#/bin/rm -f ${image}*.ps
