#!/bin/csh
#
### test for helical programs.
## image 1
./hlxsimul1.com
./fftrans.com hlx1.img hlx1.trn
./trnout.com hlx1.trn /dev/null
./hlxdump1.com
#gs llplot.ps
./hlxdump1s.com

## image 2
./hlxsimul2.com
./fftrans.com hlx2.img hlx2.trn
./trnout.com hlx2.trn /dev/null
./hlxdump2.com
./hlxdump2s.com

### fit and average 2 sides
./hlxfit.com
./hlxavg.com
./hlxfourh.com

### Ximdisp to display the final map helix.lig therefore cannot be run in background
#./hlxdisplay.com

